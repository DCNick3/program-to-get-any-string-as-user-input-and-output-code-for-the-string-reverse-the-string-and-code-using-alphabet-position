# Program to get any string as user input and output code for the string reverse the string and code using alphabet position
 
## Description
This project takes a string, reverses it, encodes using alphabet positions and prints the result. All of that is done in python3.

![demo-screenshot](https://i.imgur.com/wkt9dvP.png)

## Motivations

This program comes in handy if you are trying to get full points for Lab 2 in the Software Systems Analysis and Design course in Innopolis University. Otherwise, honestly, it is quite useless.


## How to install and run

### How to get python

First and foremost you will need to get python for your system. Follow [this guide](https://realpython.com/installing-python/) for more instructions for your system.

If you happen to use Windows follow [this guide](https://geek-university.com/python/add-python-to-the-windows-path/) to add Python to PATH.

### Install this project

Run this in terminal:

```bash
pip install program-to-get-any-string-as-user-input-and-output-code-for-the-string-reverse-the-string-and-code-using-alphabet-position
```
If you have no idea what terminal is, read guides lower

### Run this project

Run this in terminal
```bash
program-to-get-any-string-as-user-input-and-output-code-for-the-string-reverse-the-string-and-code-using-alphabet-position
```

After that you type your string in pure English and press Enter. 

### How to open terminal:  

For linux you probably already know how to do it. Follow [this guide](https://askubuntu.com/questions/38162/what-is-a-terminal-and-how-do-i-open-and-use-it) for ubuntu or debian. If you have other flavours it will most likely work too. 

If you have Windows use [this guide](https://www.wikihow.com/Open-Terminal-in-Windows) to help you. 

If you have MacOS use [this guide](https://support.apple.com/guide/terminal/open-or-quit-terminal-apd5265185d-f365-44cb-8b09-71a064a42125/mac).

After you’ve opened your terminal of choice, you can just install it with pip.



## Contributor list
 
Contacts of the contributors:  
* [Nikita Strygin](t.me/DCNick3) [n.strygin@innopolis.university](mailto:n.strygin@innopolis.university)
* [Vyacheslav Sergeev](t.me/unb0und) [vy.sergeev@innopolis.university](mailto:vy.sergeev@innopolis.university)
* [Daniil Arapov](t.me/arantirar) [d.arapov@innopolis.university](mailto:d.arapov@innopolis.university)
 
## How to contribute
 
You don’t. It is fine as it is. 

## Licence - GPLv3

![GPLv3](https://i.imgur.com/0ibrPsO.png)
